package multiton.design.pattern;

/**
 *
 * @author konstantinos
 */
public class MultitonDesignPatternTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(CodeListsFactory.Version1.INSTANCE_A_V1.getName());
        System.out.println(CodeListsFactory.Version1.INSTANCE_B_V1.getName());
        System.out.println(CodeListsFactory.Version2.INSTANCE_A_V2.getName());
        System.out.println(CodeListsFactory.Version2.INSTANCE_A_V2.getName());
    }

}
