package multiton.design.pattern;

/**
 *
 * @author Konstantinos Raptis
 */
public final class CodeListsFactory {

    private CodeListsFactory() {

    }

    /**
     * CodeLists version 1.0.2
     * 
     */
    public static final class Version1 {

        public static final CodeLists INSTANCE_A_V1 = CodeListsV1Impl.getInstance(CodeListsV1Impl.INSTANCE_A_V1.getName());

        public static final CodeLists INSTANCE_B_V1 = CodeListsV1Impl.getInstance(CodeListsV1Impl.INSTANCE_B_V1.getName());

    }

    /**
     * CodeLists version 2.0.0
     * 
     */
    public static final class Version2 {

        public static final CodeLists INSTANCE_A_V2 = CodeListsV2Impl.getInstance(CodeListsV2Impl.INSTANCE_A_V2.getName());

        public static final CodeLists INSTANCE_B_V2 = CodeListsV2Impl.getInstance(CodeListsV2Impl.INSTANCE_B_V2.getName());

    }

}
