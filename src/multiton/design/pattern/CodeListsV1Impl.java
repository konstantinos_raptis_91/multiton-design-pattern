package multiton.design.pattern;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Konstantinos Raptis
 */
public enum CodeListsV1Impl implements CodeLists {
    
    INSTANCE_A_V1("Instance A V1"),
    
    INSTANCE_B_V1("Instance B V1");

    private final static Map<String, CodeLists> codeLists;
    
    private final String name;
    
    static {
        codeLists = new ConcurrentHashMap<>();
        codeLists.put(INSTANCE_A_V1.getName(), INSTANCE_A_V1);
        codeLists.put(INSTANCE_B_V1.getName(), INSTANCE_B_V1);
    }
    
    private CodeListsV1Impl(String name) {
        this.name = name;
    }
    
    public static CodeLists getInstance(String name) {
        return codeLists.get(name);
    }
    
    @Override
    public String getName() {
        return name;
    }
    
}
