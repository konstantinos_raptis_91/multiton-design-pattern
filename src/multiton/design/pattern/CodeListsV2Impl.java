package multiton.design.pattern;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Konstantinos Raptis
 */
public enum CodeListsV2Impl implements CodeLists {
    
    INSTANCE_A_V2("Instance A V2"),
    
    INSTANCE_B_V2("Instance B V2");
    
    private final static Map<String, CodeLists> codeLists;
    
    private final String name;
    
    static {
        codeLists = new ConcurrentHashMap<>();
        codeLists.put(INSTANCE_A_V2.getName(), INSTANCE_A_V2);
        codeLists.put(INSTANCE_B_V2.getName(), INSTANCE_B_V2);
    }
    
    private CodeListsV2Impl(String name) {
        this.name = name;
    }
    
    public static CodeLists getInstance(String name) {
        return codeLists.get(name);
    }
    
    @Override
    public String getName() {
        return name;
    }
    
}
