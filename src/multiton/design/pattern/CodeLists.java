package multiton.design.pattern;

/**
 *
 * @author Konstantinos Raptis
 */
public interface CodeLists {
    
    String getName();
    
}
